#!/bin/bash

mkdir ~/.aws
cat << EOF >> /root/.aws/credentials
[test]
aws_access_key_id=$AWS_ACCESS_KEY
aws_secret_access_key=$AWS_SECRET_KEY
EOF

cat << EOF >> /root/.aws/config
[profile test]
region = $AWS_DEFAULT_REGION
output = table
EOF