import * as colors from 'colors';
import { init } from './init';
import { getSubnetRouteTables } from './getSubnetRouteTables';
import { getSubnetIdByCidr } from './getSubnetIdByCidr';
import { getSubnetIdByName } from './getSubnetIdByName';
import { question } from './inputs';
import { table, getBorderCharacters } from 'table';
import { program } from './commandFlags';

export async function showRoutes() {
  // import of isCidr package
  const isCidr = require('is-cidr');

  // Data for output table formatting
  const data: string[][] = [];
  // Configuration of output table formatting
  const config = {
    border: getBorderCharacters(`norc`)
  };

  await init(program.region, program.profile);

  let subnetId: string;
  if (program.subnet) {
    subnetId = program.subnet;
  } else if (program.subnetCidr) {
    subnetId = await getSubnetIdByCidr(program.subnetCidr);
  } else if (program.subnetName) {
    subnetId = await getSubnetIdByName(program.subnetName);
  } else {
    const input = await question(
      // tslint:disable-next-line
      "Enter a subnet ID, CIDR, or name to see it's associated route tables: "
    );
    // If input is subnet ID, set that, else try to get subnet ID by CIDR
    if (input.includes('subnet-')) {
      subnetId = input;
    } else if (isCidr(input)) {
      subnetId = await getSubnetIdByCidr(input);
    } else {
      subnetId = await getSubnetIdByName(input);
    }
  }
  console.log(
    `\nThe following route table is associated with ${colors.bold.green(
      subnetId
    )}:\n`
  );
  const result = await getSubnetRouteTables(subnetId);
  result.forEach(routeTable => {
    if (routeTable.RouteTableId) {
      if (routeTable.Tags) {
        // Filter down to Name tag
        const tags = routeTable.Tags.find(tag => tag.Key === 'Name');
        // If tags is not undefined print the route table Name
        if (tags && tags.Value) {
          console.log(
            ` ${colors.bold.green(
              routeTable.RouteTableId
            )} '${colors.bold.green(tags.Value)}'`
          );
        } else {
          console.log(` ${colors.bold.green(routeTable.RouteTableId)}`);
        }
      }
    }

    if (routeTable.Routes) {
      data.push([
        colors.bold('Route'),
        colors.bold('Status'),
        colors.bold('Target')
      ]);
      // Maybe we can add optional colors
      routeTable.Routes.map(route => {
        if (route.DestinationCidrBlock && route.State) {
          const routeTableEntry = [route.DestinationCidrBlock, route.State];
          if (route.GatewayId) {
            routeTableEntry.push(route.GatewayId);
          } else if (route.NatGatewayId) {
            routeTableEntry.push(route.NatGatewayId);
          } else if (route.NetworkInterfaceId) {
            routeTableEntry.push(route.NetworkInterfaceId);
          } else if (route.TransitGatewayId) {
            routeTableEntry.push(route.TransitGatewayId);
          } else if (route.VpcPeeringConnectionId) {
            routeTableEntry.push(route.VpcPeeringConnectionId);
          } else if (route.InstanceId) {
            routeTableEntry.push(route.InstanceId);
          } else if (route.DestinationPrefixListId) {
            routeTableEntry.push(route.DestinationPrefixListId);
          }
          data.push(routeTableEntry);
        }
      });
      console.log(table(data, config));
    } else {
      // Error on absence of routes
      console.error('No routes in table!');
    }
  });
  process.exit();
}
