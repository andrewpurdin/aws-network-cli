import * as aws from 'aws-sdk';

export async function getSubnetRouteTables(
  subnetId: string
): Promise<aws.EC2.RouteTable[]> {
  // Client to interact with EC2 API
  const ec2 = new aws.EC2();

  // Parameters for DescribeRouteTables API Call
  const routeTableParameters: aws.EC2.DescribeRouteTablesRequest = {
    Filters: [
      {
        Name: 'association.subnet-id',
        Values: [subnetId]
      }
    ]
  };

  // Create API call to describe route tables based on parameters above and return promise
  let output: aws.EC2.RouteTable[];

  const resolvedResponse = await ec2
    .describeRouteTables(routeTableParameters)
    .promise();

  // Handle error
  if (resolvedResponse.$response.error) {
    console.error('Error: ', resolvedResponse.$response.error);
  }

  // Build an object for viewing
  if (resolvedResponse.RouteTables && resolvedResponse.RouteTables.length > 0) {
    output = resolvedResponse.RouteTables;
  } // Handle exception of lack of association by showing main rtb
  else {
    // Get VPC Id to filter down main RTB
    const describeSubnet: aws.EC2.DescribeSubnetsRequest = {
      Filters: [
        {
          Name: 'subnet-id',
          Values: [subnetId]
        }
      ]
    };

    const subnetsResponse = await ec2.describeSubnets(describeSubnet).promise();
    // Filter for only main RTB in the associated VPC
    let vpcId;
    if (subnetsResponse && subnetsResponse.Subnets) {
      vpcId = subnetsResponse.Subnets[0].VpcId;
    }
    if (vpcId) {
      const mainRouteTableParameters: aws.EC2.DescribeRouteTablesRequest = {
        Filters: [
          {
            Name: 'association.main',
            Values: ['true']
          },
          {
            Name: 'vpc-id',
            Values: [vpcId]
          }
        ]
      };
      // Try API call for main rtb
      const mainRrouteTableResolvedResponse = await ec2
        .describeRouteTables(mainRouteTableParameters)
        .promise();
      // handle any errors
      if (mainRrouteTableResolvedResponse.$response.error) {
        console.error('Error: ', resolvedResponse.$response.error);
      }
      // Build output
      if (mainRrouteTableResolvedResponse.RouteTables) {
        console.log(
          '\nNo explicit associations found for subnet. Main route table will be listed instead:\n'
        );
        output = mainRrouteTableResolvedResponse.RouteTables;
      } else {
        // I don't believe this should ever be reachable
        throw new Error('No route tables found');
      }
    } else {
      throw new Error('VPC Id Could not be found from subnet Id');
    }
  }
  return output;
}
