import { Command } from 'commander';
import { showRoutes } from './showRoutes';
import { showRouteTableRoutes } from './showRouteTableRoutes';

export const program = new Command();
// sets the version and registers the --version flag to display it
program.version('0.0.3');

program
  .option('-r --region [region]', 'input the aws region to use')
  .option('-p --profile [profile]', 'input the aws profile to use');
program
  .command('show-routes', { isDefault: true })
  .description('Lists routes for a given subnet')
  .action(() => {
    showRoutes();
  })
  .option(
    '-s --subnet [subnet]',
    'input a subnet by id to display the route table for'
  )
  .option(
    '-y --subnet-cidr [cidr]',
    'input a subnet by cidr to display the route table for'
  )
  .option(
    '-n --subnet-name [name]',
    'input a subnet by name to display the route table for'
  );
program
  .command('show-rtb-routes')
  .description('Lists routes for a given route table')
  .action(() => {
    showRouteTableRoutes();
  })
  .option(
    '-i --rtb-id [rtbId]',
    'input a route table id to display the associated routes'
  )
  .option(
    '-n --rtb-name [rtbName]',
    'input a route table by name to display the associated routes'
  );
