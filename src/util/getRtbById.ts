import * as aws from 'aws-sdk';
export async function getRtbById(rtbId: string): Promise<aws.EC2.RouteTable> {
  // Client to interact with EC2 API
  const ec2 = new aws.EC2();

  const requestParams: aws.EC2.DescribeRouteTablesRequest = {
    Filters: [
      {
        Name: 'route-table-id',
        Values: [rtbId]
      }
    ]
  };
  // Variable for eventual output
  let output: aws.EC2.RouteTable = {};

  try {
    const resolvedResponse = await ec2
      .describeRouteTables(requestParams)
      .promise();

    // Handle error
    if (resolvedResponse.$response.error) {
      console.error('Error: ', resolvedResponse.$response.error);
    }

    if (
      resolvedResponse.RouteTables &&
      resolvedResponse.RouteTables[0] &&
      resolvedResponse.RouteTables[0].RouteTableId
    ) {
      output = resolvedResponse.RouteTables[0];
    } else {
      throw new Error(
        `Invalid input 
       Either the input format is incorrect or the desired route table does not exist in this region
       Route table IDs should be entered in the following format: rtb-xxxxxxxxxxxxxxxx`
      );
    }
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
  return output;
}
