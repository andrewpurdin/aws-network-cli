import * as aws from 'aws-sdk';

export async function getRtbIdByName(name: string): Promise<string> {
  // Client to interact with EC2 API
  const ec2 = new aws.EC2();

  // Variable for eventual output
  let output: string = '';
  // Variable for route table IDs in case there are multiple matches
  const rtbIds: { name: string; id: string }[] = [];
  // Variable to break out of loop if exact match is found
  let finished = false;

  try {
    const resolvedResponse = await ec2.describeRouteTables().promise();
    // for each route table
    // check for exact match, if it is, save as output and break
    // else, check for partial match, if it is, save to list of partial matches
    // on the last iteration, check the list of partial matches, if one result, save it as output
    // if more than one result, print the list and throw an error to ask for more specific input
    if (resolvedResponse.RouteTables) {
      resolvedResponse.RouteTables.forEach((rtb, index) => {
        if (finished === true) {
          return;
        }
        const nameTag = rtb.Tags!.filter(tag => tag.Key === 'Name');
        if (
          nameTag[0] &&
          nameTag[0].Value &&
          rtb.RouteTableId &&
          nameTag[0].Value === name
        ) {
          output = rtb.RouteTableId;
          finished = true;
        } else if (
          nameTag[0] &&
          nameTag[0].Value &&
          rtb.RouteTableId &&
          nameTag[0].Value.includes(name)
        ) {
          rtbIds.push({ name: nameTag[0].Value, id: rtb.RouteTableId });
        }
        // If we are  on the last iteration
        if (index === resolvedResponse.RouteTables!.length - 1) {
          // and if there is only one matching value
          if (rtbIds.length === 1) {
            output = rtbIds[0].id;
          } else if (rtbIds.length === 0) {
            throw new Error(
              `
           Either the input format is incorrect or a matching route table does not exist in this region
           `
            );
          } else {
            console.log('Matching route tables:');
            console.log(rtbIds);
            throw new Error(
              `\nRoute table name search returned more than one result, please use a more specific name\n`
            );
          }
        }
      });
    } else {
      throw new Error(
        `
     Either the input format is incorrect or a matching route table does not exist in this region
     `
      );
    }
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
  return output;
}
