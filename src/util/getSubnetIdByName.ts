import * as aws from 'aws-sdk';

export async function getSubnetIdByName(name: string): Promise<string> {
  // Client to interact with EC2 API
  const ec2 = new aws.EC2();

  // Variable for eventual output
  let output: string = '';
  // Variable for subnet IDs in case there are multiple matches
  const subnetIds: { name: string; id: string }[] = [];
  // Variable to break out of loop if exact match is found
  let finished = false;

  try {
    const resolvedResponse = await ec2.describeSubnets().promise();
    // for each subnet
    // check for exact match, if it is, save as output and break
    // else, check for partial match, if it is, save to list of partial matches
    // on the last iteration, check the list of partial matches, if one result, save it as output
    // if more than one result, print the list and throw an error to ask for more specific input
    if (resolvedResponse.Subnets) {
      resolvedResponse.Subnets.forEach((subnet, index) => {
        if (finished === true) {
          return;
        }
        const nameTag = subnet.Tags!.filter(tag => tag.Key === 'Name');
        if (
          nameTag[0] &&
          nameTag[0].Value &&
          subnet.SubnetId &&
          nameTag[0].Value === name
        ) {
          output = subnet.SubnetId;
          finished = true;
        } else if (
          nameTag[0] &&
          nameTag[0].Value &&
          subnet.SubnetId &&
          nameTag[0].Value.includes(name)
        ) {
          subnetIds.push({ name: nameTag[0].Value, id: subnet.SubnetId });
        }
        // If we are  on the last iteration
        if (index === resolvedResponse.Subnets!.length - 1) {
          // and if there is only one matching value
          if (subnetIds.length === 1) {
            output = subnetIds[0].id;
          } else if (subnetIds.length === 0) {
            throw new Error(
              `
           Either the input format is incorrect or a matching subnet does not exist in this region
           `
            );
          } else {
            console.log('Matching Subnets:');
            console.log(subnetIds);
            throw new Error(
              `\nSubnet name search returned more than one result, please use a more specific name\n`
            );
          }
        }
      });
    } else {
      throw new Error(
        `
     Either the input format is incorrect or a matching subnet does not exist in this region
     `
      );
    }
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
  return output;
}
