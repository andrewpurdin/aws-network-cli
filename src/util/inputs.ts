import * as readline from 'readline';

// Create the readline interface to prompt users for input
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Function that prompts a user for an input and returns a promise string of that input
export const question = (input: string) => {
  return new Promise<string>((resolve, reject) => {
    rl.question(input, result => {
      resolve(result);
    });
  });
};
