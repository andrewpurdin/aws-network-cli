import * as aws from 'aws-sdk';

export async function getSubnetIdByCidr(cidr: string): Promise<string> {
  // Client to interact with EC2 API
  const ec2 = new aws.EC2();

  const requestParams: aws.EC2.DescribeSubnetsRequest = {
    Filters: [
      {
        Name: 'cidr-block',
        Values: [cidr]
      }
    ]
  };
  // Variable for eventual output
  let output: string = '';

  try {
    const resolvedResponse = await ec2.describeSubnets(requestParams).promise();

    if (
      resolvedResponse.Subnets &&
      resolvedResponse.Subnets[0] &&
      resolvedResponse.Subnets[0].SubnetId
    ) {
      output = resolvedResponse.Subnets[0].SubnetId!;
    } else {
      throw new Error(
        `Invalid input 
       Either the input format is incorrect or the desired subnet does not exist in this region
       Subnet IDs should be entered in the following format: subnet-xxxxxxxxxxxxxxxx
       CIDR ranges should be entered in the following format: 10.0.0.0/8`
      );
    }
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
  return output;
}
