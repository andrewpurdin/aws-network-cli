import { table, getBorderCharacters } from 'table';
import { getRtbById } from './getRtbById';
import { getRtbIdByName } from './getRtbIdByName';
import { init } from './init';
import { program } from './commandFlags';
import { question } from './inputs';
import * as colors from 'colors';

export async function showRouteTableRoutes() {
  // Data for output table formatting
  const data: string[][] = [];

  // Configuration of output table formatting
  const config = {
    border: getBorderCharacters(`norc`)
  };

  // Inital values for region and profile
  await init(program.region, program.profile);

  let rtbId: string;
  if (program.rtbId) {
    rtbId = program.rtbId;
  } else if (program.rtbName) {
    rtbId = await getRtbIdByName(program.rtbName);
  } else {
    const input = await question(
      // tslint:disable-next-line
      "Enter a route table ID or name to see it's associated routes: "
    );
    rtbId = input.includes('rtb-') ? input : await getRtbIdByName(input);
  }

  const result = await getRtbById(rtbId);
  if (result.RouteTableId) {
    if (result.Tags) {
      // Filter down to Name tag
      const tags = result.Tags.find(tag => tag.Key === 'Name');
      // If tags is not undefined print the route table Name
      if (tags && tags.Value) {
        console.log(
          ` \n${colors.bold.green(result.RouteTableId)} '${colors.bold.green(
            tags.Value
          )}'\n`
        );
      } else {
        console.log(` \n${colors.bold.green(result.RouteTableId)}\n`);
      }
    }
  }

  if (result.Routes) {
    data.push([
      colors.bold('Route'),
      colors.bold('Status'),
      colors.bold('Target')
    ]);
    // Maybe we can add optional colors
    result.Routes.map(route => {
      if (route.DestinationCidrBlock && route.State) {
        const routeTableEntry = [route.DestinationCidrBlock, route.State];
        if (route.GatewayId) {
          routeTableEntry.push(route.GatewayId);
        } else if (route.NatGatewayId) {
          routeTableEntry.push(route.NatGatewayId);
        } else if (route.NetworkInterfaceId) {
          routeTableEntry.push(route.NetworkInterfaceId);
        } else if (route.TransitGatewayId) {
          routeTableEntry.push(route.TransitGatewayId);
        } else if (route.VpcPeeringConnectionId) {
          routeTableEntry.push(route.VpcPeeringConnectionId);
        } else if (route.InstanceId) {
          routeTableEntry.push(route.InstanceId);
        } else if (route.DestinationPrefixListId) {
          routeTableEntry.push(route.DestinationPrefixListId);
        }
        data.push(routeTableEntry);
      }
    });
    console.log(table(data, config));
  } else {
    // Error on absence of routes
    console.error('No routes in table!');
  }
  process.exit();
}
