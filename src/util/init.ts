import * as aws from 'aws-sdk';
import { question } from './inputs';

export async function init(region?: string, profile?: string) {
  // set the AWS region to use
  if (region === undefined) {
    region = await question('Enter the AWS region to run the query against: ');
  }

  aws.config.update({ region });

  // Set Profile to use
  if (profile === undefined) {
    profile = await question(
      'Enter the AWS account profile you would like to use: '
    );
  }

  const credentials = new aws.SharedIniFileCredentials({
    profile
  });
  aws.config.credentials = credentials;
}
