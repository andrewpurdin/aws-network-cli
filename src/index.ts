import { program } from './util/commandFlags';

// Parse command input
program.parse(process.argv);

if (program.args.length === 0) {
  console.log(
    '\nA subcommand is required. Enter --help for a full list of options'
  );
  process.exit(0);
}
