import { expect } from 'chai';
import { testValues } from '../config';
import { init } from '../../util/init';
import { getSubnetRouteTables } from '../../util/getSubnetRouteTables';
import { getSubnetIdByCidr } from '../../util/getSubnetIdByCidr';
import { getSubnetIdByName } from '../../util/getSubnetIdByName';

describe('# Unit Tests', () => {
  it('example subnet id should produce the associated route table', async () => {
    await init(testValues.spec.region, testValues.spec.profile);

    const resolvedRtb = await getSubnetRouteTables(
      testValues.spec.subnetSpec.subnetId
    );

    expect(resolvedRtb[0].RouteTableId).to.equal(
      testValues.spec.subnetSpec.expectedRouteTableId
    );
  });
  it('unassociated subnet id should produce the main route table', async () => {
    await init(testValues.spec.region, testValues.spec.profile);

    const resolvedRtb = await getSubnetRouteTables(
      testValues.spec.subnetSpec.unassociatedSubnetId
    );

    expect(resolvedRtb[0].RouteTableId).to.equal(
      testValues.spec.subnetSpec.expectedMainRouteTableId
    );
  });
  it('example cidr should produce the associated route table', async () => {
    await init(testValues.spec.region, testValues.spec.profile);

    const subnetId = await getSubnetIdByCidr(
      testValues.spec.subnetSpec.subnetCidr
    );

    const resolvedRtb = await getSubnetRouteTables(subnetId);

    expect(resolvedRtb[0].RouteTableId).to.equal(
      testValues.spec.subnetSpec.expectedRouteTableId
    );
  });
  it('example subnet name should produce the associated route table', async () => {
    await init(testValues.spec.region, testValues.spec.profile);

    const subnetName = await getSubnetIdByName(
      testValues.spec.subnetSpec.subnetName
    );

    const resolvedRtb = await getSubnetRouteTables(subnetName);

    expect(resolvedRtb[0].RouteTableId).to.equal(
      testValues.spec.subnetSpec.expectedRouteTableId
    );
  });
});
