export interface Config {
  readonly spec: Spec;
}

interface Spec {
  readonly profile: string;
  readonly region: string;
  readonly subnetSpec: SubnetRouteTableAssociation;
}

interface SubnetRouteTableAssociation {
  readonly subnetId: string;
  readonly unassociatedSubnetId: string;
  readonly subnetCidr: string;
  readonly subnetName: string;
  readonly expectedRouteTableId: string;
  readonly expectedMainRouteTableId: string;
}
