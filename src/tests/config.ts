import { Config } from './types';

export const testValues: Config = {
  spec: {
    profile: 'test',
    region: 'us-west-2',
    subnetSpec: {
      subnetId: 'subnet-11aef24b',
      unassociatedSubnetId: 'subnet-e7bbc3cf',
      subnetCidr: '172.31.0.0/20',
      subnetName: 'test',
      expectedRouteTableId: 'rtb-0b3cb9656f3b20366',
      expectedMainRouteTableId: 'rtb-85d90cfe'
    }
  }
};
